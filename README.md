#Sf-covoiturage
Zinedeen Davousse et Matthis Pinon <br>
Nous n'avons pas réussi à mettre en place un docker pour symfony, cependant pour lancer un serveur symfony, il suffit d'avoir php, composer et yarn d'installer sur votre pc

##Installation
Afin d'obtenir composer :
```
#Il faut télécharger composer
$ sudo curl -s https://getcomposer.org/installer | php

#Et ensuite e déplacer dans votre dossier /usr/local/bin/composer
$ sudo mv composer.phar /usr/local/bin/composer
```
Afin d'obtenir yarn :
```
$ sudo apt update && sudo apt install yarn
```

##Mise en place du serveur
Une fois dans la racine de l'application ``sf-covoiturage/`` : /!\ Il faut avoir son serveur sql d'allumer
```
#Exécuter ces commandes dans l'ordre :
$ composer install
$ php bin/console doctrine:database:create
$ php bin/console doctrine:migrations:migrate
$ yarn install
$ yarn dev
$ php -S localhost:8000 -t public
```
Votre serveur est maintenant lancer sur le port 8000 <br>
Si vous avez des questions, n'hésitez pas à envoyer un mail à Zinedeen : zinedeen.mouhamad-davousse@utbm.fr