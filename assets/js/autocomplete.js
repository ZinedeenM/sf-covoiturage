import 'jquery-ui/ui/widgets/autocomplete';
import '../css/autocomplete.css';
import 'bootstrap-datepicker';
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.css'

$(document).ready(function (){
    //Calendar
    $('#new_trajet_trajet_dateDepart').datepicker({
        format: "dd-mm-yyyy",
        todayBtn: "linked",
        language: "fr",
        todayHighlight: true
    });

    $('.autocomplete-place').autocomplete({
        source: function (request, response){
            $.ajax({
                url: 'https://api-adresse.data.gouv.fr/search/',
                data: {
                    q: request.term,
                },
                dataType: 'json',
                method: 'get'
            }).done(function (data){
                let resp = [];
                let features = data.features;
                for (let i = 0; i < Math.min(3, features.length); i++) {
                    let properties = features[i].properties;
                    resp[i] = properties.label;
                }
                response(resp);
            });
        },
        minLength: 3
    });

})
