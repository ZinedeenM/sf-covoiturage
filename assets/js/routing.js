import 'leaflet';
import 'leaflet/dist/leaflet.css'
import 'leaflet-routing-machine';
import 'leaflet-routing-machine/dist/leaflet-routing-machine.css';
import '../css/routing.css';
import {remove} from "leaflet/src/dom/DomUtil";

let routing;
let map;

$(window).ready(function () {
    map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(map);


    // routing.on('routesfound', function (e){
    //     console.log(e.routes[0].summary);
    // })

    $('#new_trajet_lieuDepart').change(function (e) {
        let $input = $(e.currentTarget);
        $.ajax({
            url: 'https://api-adresse.data.gouv.fr/search/',
            data: {
                q: $input.val(),
            },
            dataType: 'json',
            method: 'get'
        }).done(function (resp) {
            if ($input.val() === resp.features[0].properties.label) {
                $('#map').attr('data-startx', resp.features[0].geometry.coordinates[1]).attr('data-starty', resp.features[0].geometry.coordinates[0]);
            } else {
                $('#map').attr('data-startx', "").attr('data-starty', "")
            }
            updateItinnary()
        })
    })

    $('#new_trajet_lieuArrivee').change(function (e) {
        let $input = $(e.currentTarget);
        $.ajax({
            url: 'https://api-adresse.data.gouv.fr/search/',
            data: {
                q: $input.val(),
            },
            dataType: 'json',
            method: 'get'
        }).done(function (resp) {
            if ($input.val() === resp.features[0].properties.label) {
                $('#map').attr('data-endx', resp.features[0].geometry.coordinates[1]).attr('data-endy', resp.features[0].geometry.coordinates[0]);
            } else {
                $('#map').attr('data-endx', "").attr('data-endy', "")
            }
            updateItinnary();
        })
    })
})

function updateItinnary() {
    if (routing) {
        routing.getPlan().spliceWaypoints(0, routing.getWaypoints().length);
    }

    if ($('#map').attr('data-endx') !== "" && $('#map').attr('data-startx') !== "") {
        routing = L.Routing.control({
            waypoints: [
                L.latLng($('#map').attr('data-startx'), $('#map').attr('data-starty')),
                L.latLng($('#map').attr('data-endx'), $('#map').attr('data-endy'))
            ],
            draggableWaypoints: false,
            addWaypoints: false
        }).addTo(map);
    }
    if (routing) {
        routing.hide();
        routing.on('routesfound', function (e) {
            $('#new_trajet_trajet_distance').val(e.routes[0].summary.totalDistance);
            $('#heure_arrivee').val(e.routes[0].summary.totalTime);
        })

    }
    $('.leaflet-marker-icon').attr('src', "https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Map_marker.svg/512px-Map_marker.svg.png");
}