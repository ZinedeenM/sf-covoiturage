<?php


namespace App\Controller;

use App\Entity\Trajet;
use App\Entity\TrajetFavoris;
use App\Entity\User;
use App\Entity\Vehicule;
use App\Form\NewTrajetType;
use App\Form\TrajetType;
use App\Form\VehiculeType;
use App\Helper\PlaceHelper;
use App\Repository\LieuRepository;
use App\Repository\TrajetRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/trajet", name="trip_")
 */
class TrajetController extends AbstractController
{
    /**
     * @Route("/enregistrement", name="register", methods={"POST", "GET"})
     * @IsGranted("ROLE_CONDUCTOR")
     */
    function tripRegister (Request $request, PlaceHelper $placeHelper, LieuRepository $lieuRepository)
    {
        $newTrajet = ["trajet" => new Trajet(), "isFav" => null];

        $form = $this->createForm(NewTrajetType::class, $newTrajet, ['actualUser' => $this->getUser()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $newTrajet = $form->getData();
            /** @var Trajet $trajet */
            $trajet = $newTrajet["trajet"];

            $lieuDepartString = $newTrajet['lieuDepart'];
            $lieuArriveeString = $newTrajet['lieuArrivee'];

            if (! (($lieuDepart = $lieuRepository->findOneBy(['adresse' => $lieuDepartString]) ?? $placeHelper->placeFromString($lieuDepartString))
                && $lieuArrivee = $lieuRepository->findOneBy(['adresse' => $lieuArriveeString]) ?? $placeHelper->placeFromString($lieuArriveeString))){
                $this->addFlash("danger", $lieuDepart ? "Le lieu de départ est incorrecte" : "Le lieu d'arrivée n'est pas correcte");
                throw new \Exception("Les lieux ne sont pas corrects");
            }

            /** @var User $user */
            $user = $this->getUser();
            $trajet->setConducteur($user);
            $trajet->setLieuDepart($lieuDepart);
            $trajet->setLieuArrivee($lieuArrivee);
            $trajet->addCovoitureur($user);
            $user->addMesTrajet($trajet);

            $trajet->setPrix(($trajet->getDistance()/100000)*$trajet->getVoiture()->getConsommation());

            $isFav = $newTrajet["isFav"];

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($trajet);
            $entityManager->persist($user);
            $entityManager->persist($lieuDepart);
            $entityManager->persist($lieuArrivee);

            if ($isFav) {
                $trajetFav = new TrajetFavoris();
                $trajetFav->copyTrip($trajet);
                $trajetFav->setUser($trajet->getConducteur());
                $trajetFav->setReccurence("");
                $entityManager->persist($trajetFav);
            }
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render("form/trajet.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/recherche", name="search", methods={"GET"})
     */
    public function searchTrip(Request $request, PlaceHelper $placeHelper,
                               LieuRepository $lieuRepository, TrajetRepository $trajetRepository){
        if (! (($lieuDepart = $lieuRepository->findOneBy(['adresse' => $request->query->get('lieuDepart')]) ?? $placeHelper->placeFromString($request->query->get('lieuDepart')))
            && $lieuArrivee = $lieuRepository->findOneBy(['adresse' => $request->query->get('lieuArrivee')]) ?? $placeHelper->placeFromString($request->query->get('lieuArrivee')))){
            $this->addFlash("danger", $lieuDepart ? "Le lieu de départ est incorrecte" : "Le lieu d'arrivée n'est pas correcte");
            return $this->redirectToRoute("home");
        }

        $dateDepart = new \DateTime($request->query->get('dateDepart'));
        $heureDepart = null;
        $trajetsRes = $trajetRepository->search($lieuDepart, $lieuArrivee, $dateDepart, $lieuRepository);

        return new Response($this->renderView('search.html.twig', [
            'listTrajets' => $trajetsRes,
        ]));
    }

    /**
     * @Route("/pending/{trajet}", name="pending", methods={"GET"})
     */
    public function userPending(Trajet $trajet){
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $attentes = array_merge($trajet->getAttentes()??[],[$currentUser->getId()]);

        $trajet->setAttentes($attentes);

        $em = $this->getDoctrine()->getManager();
        $em->persist($trajet);
        $em->flush();

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/accept/{trajet}/{user}", name="acceptUser", methods={"GET"})
     */
    public function acceptUser(Trajet $trajet, User $user){
        $trajet->addCovoitureur($user);
        $user->addMesTrajet($trajet);
        $attentes = $trajet->getAttentes();
        $length = count($attentes);
        for ($i=0; $i < $length; $i+=1){
            if ($attentes[$i] === $user->getId())
                array_splice($attentes, $i, 1);
        }
        $trajet->setAttentes($attentes);

        $em = $this->getDoctrine()->getManager();
        $em->persist($trajet);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('home');
    }


}