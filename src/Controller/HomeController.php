<?php

namespace App\Controller;

use App\Entity\Lieu;
use App\Entity\Trajet;
use App\Entity\TrajetFavoris;
use App\Entity\User;
use App\Form\UtilisateurType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET", "POST"})
     */
    function home(Request $request, UserPasswordEncoderInterface $encoder, AuthenticationUtils $authenticationUtils)
    {
        $connected = null === $request->query->get('connected') ? null : $request->query->get('connected');

        $user = new User();
        $form = $this->createForm(UtilisateurType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash("success", "Le compte a bien été créé.");

                return $this->redirectToRoute('home');
            }catch (\Exception $e){
                $this->addFlash('danger', 'une erreur est survenue : '.$e->getMessage());
            }
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($connected && $this->getUser() && in_array('ROLE_USER', $this->getUser()->getRoles()))
            $this->addFlash("success", "Vous avez bien été connecté !");
        if (null !== $connected && !$connected)
            $this->addFlash("danger", "Echec de connexion veuillez réessayer");


        /** @var User $actualUser */
        if ($actualUser = $this->getUser()){
            $trajetRepo = $this->getDoctrine()->getRepository(Trajet::class);
            $listTrajetFavs = [];
            foreach ($actualUser->getFavs() as $fav){
                $listTrajetFavs = array_merge($listTrajetFavs,
                    $trajetRepo->searchFav($fav->getLieuDepart(), $fav->getLieuArrivee(),
                    $this->getDoctrine()->getRepository(Lieu::class)));
            }
        }

        dump($user->getMesTrajets());

        $labeledUsers = [];
        foreach ($this->getDoctrine()->getRepository(User::class)->findAll() as $queriedUser){
            $labeledUsers[$queriedUser->getId()] = $queriedUser;
        }

        return new Response($this->renderView('homepage.html.twig', [
            'homepage' => true,
            'form' => $form->createView(),
            'last_username' => $lastUsername,
            'error' => $error,
            'listFavs' => $listTrajetFavs ?? null,
            'labeledUser' => $labeledUsers,
            'mesTrajets' => $user->getMesTrajets()??null,
        ]));
    }
}