<?php


namespace App\Controller;


use App\Entity\Interet;
use App\Entity\TrajetFavoris;
use App\Entity\User;
use App\Entity\Vehicule;
use App\Form\UtilisateurType;
use App\Form\VehiculeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/utilisateur", name="user_")
 * @IsGranted("ROLE_USER")
 */
class UtilisateurController extends AbstractController
{
    /**
     * @Route("/profile/{user}", name="userPage", methods={"POST", "GET"})
     */
    function userPage(User $user, Request $request, UserPasswordEncoderInterface $encoder)
    {
        $infoInteret=$this->getDoctrine()->getRepository(Interet::class)->findAll();
        $infoTrajetFav=$this->getDoctrine()->getRepository(TrajetFavoris::class)->findBy(['user'=>$user]);
        $infoVehicule=$this->getDoctrine()->getRepository(Vehicule::class)->findBy(['user'=>$user]);

        /** @var User $actualUser */
        $actualUser = $this->getUser();
        $form = $this->createForm(UtilisateurType::class, $actualUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->updateUser($form, $encoder);
        }


        return $this->render('userPage/userPage.html.twig', [
            'infoUser'=>$user,
            'infoInteret'=>$infoInteret,
            'infoTrajetFav'=>$infoTrajetFav,
            'infoVehicule'=>$infoVehicule,
            'form'=>$form->createView(),
        ]);
    }

    /**
     * @Route("/interet/update", name="updateInteret", methods={"POST"})
     */
    function updateInteret(Request $request){
        $infoInteret=$this->getDoctrine()->getRepository(Interet::class)->findAll();

        /** @var User $user */
        $user = $this->getUser();

        /** @var  Interet $interet */
        foreach( $infoInteret as $interet){
            if($request->request->get($interet->getId()) ?? null){
                $user->addInterest($interet);
            }else {
                try {
                    $user->removeInterest($interet);
                } catch (\Exception $e) {}
            }
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $this->addFlash("success", "Vos intêrets ont bien été enregistrés");

        return $this->redirectToRoute('user_userPage', ['user'=>$user->getId()]);
    }

    function updateUser ($form, UserPasswordEncoderInterface $encoder){

        /** @var User $user */
        $user = $form->getData();
        $bufferUser = new User();
        $encoder->encodePassword($bufferUser, $user->getPassword());
        dump($this->getUser());
        dump($user);

        if ($this->getUser()->getPassword() === $bufferUser->getPassword()){
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash("success", "Le compte a bien été mis à jour.");

            }catch (\Exception $e){
                $this->addFlash('danger', 'une erreur est survenue : '.$e->getMessage());
            }
        }else{
            $this->addFlash('danger', 'Le code entré ne correspond pas à votre code d\'origine');
        }
        return $this->redirectToRoute('user_userPage', [
            'user'=>$user->getId(),
        ]);
    }

    /**
     * @Route("/vehicule/update/{vehicule}", name="updateVehicule", methods={"POST"})
     */
    function updateVehicule(Vehicule $vehicule, Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $request->request->get("name");

        $vehicule->setConsommation($request->request->get("consommation"));
        $vehicule->setImmatriculation($request->request->get("immatriculation"));
        $vehicule->setMarque($request->request->get("marque"));
        $vehicule->setModel($request->request->get("model"));
        $vehicule->setNbPlaces($request->request->get("nbPlaces"));
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vehicule);
            $entityManager->flush();

            $this->addFlash("success", "Le véhicule a bien été mis à jour.");

        }catch (\Exception $e){
            $this->addFlash('danger', 'une erreur est survenue : '.$e->getMessage());
        }

        return $this->redirectToRoute('user_userPage', [
            'user'=>$user->getId(),
        ]);
    }

    /**
     * @Route("/vehicule/create", name="createVehicule", methods={"POST"})
     */
    function createVehicule(Request $request)
    {
        $vehicule=new Vehicule();

        /** @var User $user */
        $user = $this->getUser();
        $request->request->get("name");

        $vehicule->setIdUtilisateur($user);
        $vehicule->setConsommation($request->request->get("consommation"));
        $vehicule->setImmatriculation($request->request->get("immatriculation"));
        $vehicule->setMarque($request->request->get("marque"));
        $vehicule->setModel($request->request->get("model"));
        $vehicule->setNbPlaces($request->request->get("nbPlaces"));
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vehicule);
            $entityManager->flush();

            $this->addFlash("success", "Le véhicule a bien été créé.");

        }catch (\Exception $e){
            $this->addFlash('danger', 'une erreur est survenue : '.$e->getMessage());
        }

        return $this->redirectToRoute('user_userPage', [
            'user'=>$user->getId(),
        ]);
    }
}