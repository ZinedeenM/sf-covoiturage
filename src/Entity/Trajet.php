<?php

namespace App\Entity;

use App\Repository\TrajetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrajetRepository::class)
 */
class Trajet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $distance;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbPassager;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDepart;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $lieuDepart;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $lieuArrivee;

    /**
     * @ORM\ManyToMany(targetEntity=Interet::class)
     */
    private $prefInteret;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $conducteur;


    /**
     * @ORM\Column(type="time")
     */
    private $heureDepart;

    /**
     * @ORM\ManyToOne(targetEntity=Vehicule::class)
     */
    private $voiture;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $attentes = [];

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="mesTrajets")
     */
    private $covoitureur;

    public function __construct()
    {
        $this->prefInteret = new ArrayCollection();
        $this->covoitureur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(float $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getNbPassager(): ?int
    {
        return $this->nbPassager;
    }

    public function setNbPassager(int $nbPassager): self
    {
        $this->nbPassager = $nbPassager;

        return $this;
    }

    public function getDateDepart(): ?\DateTimeInterface
    {
        return $this->dateDepart;
    }

    public function setDateDepart(\DateTimeInterface $dateDepart): self
    {
        $this->dateDepart = $dateDepart;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getLieuDepart(): ?Lieu
    {
        return $this->lieuDepart;
    }

    public function setLieuDepart(?Lieu $lieuDepart): self
    {
        $this->lieuDepart = $lieuDepart;

        return $this;
    }

    public function getLieuArrivee(): ?Lieu
    {
        return $this->lieuArrivee;
    }

    public function setLieuArrivee(?Lieu $lieuArrivee): self
    {
        $this->lieuArrivee = $lieuArrivee;

        return $this;
    }

    /**
     * @return Collection|Interet[]
     */
    public function getPrefInteret(): Collection
    {
        return $this->prefInteret;
    }

    public function addPrefInteret(Interet $prefInteret): self
    {
        if (!$this->prefInteret->contains($prefInteret)) {
            $this->prefInteret[] = $prefInteret;
        }

        return $this;
    }

    public function removePrefInteret(Interet $prefInteret): self
    {
        $this->prefInteret->removeElement($prefInteret);

        return $this;
    }

    public function getConducteur(): ?User
    {
        return $this->conducteur;
    }

    public function setConducteur(?User $conducteur): self
    {
        $this->conducteur = $conducteur;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getCovoitureur(): Collection
    {
        return $this->covoitureur;
    }

    public function addCovoitureur(User $covoitureur): self
    {
        if (!$this->covoitureur->contains($covoitureur)) {
            $this->covoitureur[] = $covoitureur;
        }

        return $this;
    }

    public function removeCovoitureur(User $covoitureur): self
    {
        $this->covoitureur->removeElement($covoitureur);

        return $this;
    }

    public function getHeureDepart(): ?\DateTimeInterface
    {
        return $this->heureDepart;
    }

    public function setHeureDepart(\DateTimeInterface $heureDepart): self
    {
        $this->heureDepart = $heureDepart;

        return $this;
    }

    public function getVoiture(): ?Vehicule
    {
        return $this->voiture;
    }

    public function setVoiture(?Vehicule $voiture): self
    {
        $this->voiture = $voiture;

        return $this;
    }

    public function getAttentes(): ?array
    {
        return $this->attentes;
    }

    public function setAttentes(?array $attentes): self
    {
        $this->attentes = $attentes;

        return $this;
    }
}
