<?php

namespace App\Entity;

use App\Repository\TrajetFavorisRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrajetFavorisRepository::class)
 */
class TrajetFavoris
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reccurence;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $lieuDepart;

    /**
     * @ORM\ManyToOne(targetEntity=Lieu::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $lieuArrivee;

    /**
     * @ORM\Column(type="time")
     */
    private $heureDepart;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="favs")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReccurence(): ?string
    {
        return $this->reccurence;
    }

    public function setReccurence(string $reccurence): self
    {
        $this->reccurence = $reccurence;

        return $this;
    }

    public function getLieuDepart(): ?Lieu
    {
        return $this->lieuDepart;
    }

    public function setLieuDepart(?Lieu $lieuDepart): self
    {
        $this->lieuDepart = $lieuDepart;

        return $this;
    }

    public function getLieuArrivee(): ?Lieu
    {
        return $this->lieuArrivee;
    }

    public function setLieuArrivee(?Lieu $lieuArrivee): self
    {
        $this->lieuArrivee = $lieuArrivee;

        return $this;
    }

    public function copyTrip (Trajet $trajet): self
    {
        $this->lieuDepart = $trajet->getLieuDepart();
        $this->lieuArrivee = $trajet->getLieuArrivee();
        $this->heureDepart = $trajet->getHeureDepart();
        return $this;
    }

    public function getHeureDepart(): ?\DateTimeInterface
    {
        return $this->heureDepart;
    }

    public function setHeureDepart(\DateTimeInterface $heureDepart): self
    {
        $this->heureDepart = $heureDepart;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
