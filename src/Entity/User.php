<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements \Symfony\Component\Security\Core\User\UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="date")
     */
    private $date_naissance;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAdmin;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity=Interet::class, inversedBy="users")
     */
    private $interests;

    /**
     * @ORM\OneToMany(targetEntity=Vehicule::class, mappedBy="user")
     */
    private $vehicules;

    /**
     * @ORM\OneToMany(targetEntity=TrajetFavoris::class, mappedBy="user")
     */
    private $favs;

    /**
     * @ORM\ManyToMany(targetEntity=Trajet::class, mappedBy="covoitureur")
     */
    private $mesTrajets;


    public function __construct()
    {
        $this->interests = new ArrayCollection();
        $this->vehicules = new ArrayCollection();
        $this->favs = new ArrayCollection();
        $this->mesTrajets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        if ($this->isAdmin){
            $roles[] = 'ROLE_ADMIN';
        }
        if (!$this->vehicules->isEmpty()){
            $roles[] = 'ROLE_CONDUCTOR';
        }
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->date_naissance;
    }

    public function setDateNaissance(\DateTimeInterface $date_naissance): self
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }

    public function getIsAdmin(): ?bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(bool $isAdmin): self
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * @return Collection|Interet[]
     */
    public function getInterests(): Collection
    {
        return $this->interests;
    }

    public function addInterest(Interet $interest): self
    {
        if (!$this->interests->contains($interest)) {
            $this->interests[] = $interest;
        }
        return $this;
    }

    /**
     * @return Collection|Interet[]
     */
    public function getMesTrajets(): Collection
    {
        return $this->interests;
    }

    public function addMesTrajet(Trajet $mesTrajet): self
    {
        if (!$this->mesTrajets->contains($mesTrajet)) {
            $this->mesTrajets[] = $mesTrajet;
        }
        return $this;
    }

    /**
     * @return Collection|Vehicule[]
     */
    public function getVehicules(): Collection
    {
        return $this->vehicules;
    }

    public function addVehicule(Vehicule $vehicule): self
    {
        if (!$this->vehicules->contains($vehicule)) {
            $this->vehicules[] = $vehicule;
            $vehicule->setUser($this);
        }

        return $this;
    }

    public function removeInterest(Interet $interest): self
    {
        $this->interests->removeElement($interest);

        return $this;
    }

    public function removeMesTrajet(Trajet $mesTrajet): self
    {
        $this->mesTrajets->removeElement($mesTrajet);

        return $this;
    }

    public function removeVehicule(Vehicule $vehicule): self
    {
        if ($this->vehicules->removeElement($vehicule)) {
            // set the owning side to null (unless already changed)
            if ($vehicule->getUser() === $this) {
                $vehicule->setUser(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|TrajetFavoris[]
     */
    public function getFavs(): Collection
    {
        return $this->favs;
    }

    public function addFav(TrajetFavoris $fav): self
    {
        if (!$this->favs->contains($fav)) {
            $this->favs[] = $fav;
            $fav->setUser($this);
        }

        return $this;
    }

    public function removeFav(TrajetFavoris $fav): self
    {
        if ($this->favs->removeElement($fav)) {
            // set the owning side to null (unless already changed)
            if ($fav->getUser() === $this) {
                $fav->setUser(null);
            }
        }

        return $this;
    }
}
