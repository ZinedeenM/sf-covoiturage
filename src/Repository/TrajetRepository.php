<?php

namespace App\Repository;

use App\Entity\Lieu;
use App\Entity\Trajet;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Trajet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trajet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trajet[]    findAll()
 * @method Trajet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrajetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trajet::class);
    }
   /**
    * @return Trajet[] Returns an array of Trajet objects
    */

    public function search(Lieu $lieuDepart, Lieu $lieuArrivee, \DateTime $dateDepart, LieuRepository $lieuRepository)
    {
        $qb = $this->createQueryBuilder('t');
        $listDepart = [];
        $listArrivee = [];

        foreach ($lieuRepository->findBy(['ville' => $lieuDepart->getVille()]) as $lieu)
            $listDepart[] = $lieu->getId();
        foreach ($lieuRepository->findBy(['ville' => $lieuArrivee->getVille()]) as $lieu)
            $listArrivee[] = $lieu->getId();

        $qb
            ->andWhere($qb->expr()->in('t.lieuDepart', $listDepart))
            ->andWhere($qb->expr()->in('t.lieuArrivee', $listArrivee))
            ->andWhere('t.dateDepart = :date')
            ->setParameter('date', $dateDepart->format('Y-m-d'))

        ;
        return $qb
            ->orderBy('t.heureDepart', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function searchFav(Lieu $lieuDepart, Lieu $lieuArrivee, LieuRepository $lieuRepository)
    {
        $qb = $this->createQueryBuilder('t');
        $listDepart = [];
        $listArrivee = [];

        foreach ($lieuRepository->findBy(['ville' => $lieuDepart->getVille()]) as $lieu)
            $listDepart[] = $lieu->getId();
        foreach ($lieuRepository->findBy(['ville' => $lieuArrivee->getVille()]) as $lieu)
            $listArrivee[] = $lieu->getId();

        $qb
            ->andWhere($qb->expr()->in('t.lieuDepart', $listDepart))
            ->andWhere($qb->expr()->in('t.lieuArrivee', $listArrivee))
            ->andWhere('t.dateDepart >= :date')
            ->setParameter('date', (new \DateTime('now'))->format('Y-m-d'))

        ;
        return $qb

            ->orderBy('t.dateDepart, t.heureDepart', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
//
//    public function findByCovoit(User $user, Trajet $trajet): ?Trajet
//    {
//        $qb = $this->createQueryBuilder('t');
//        return $qb
//            ->andWhere($qb->expr()->in($user->getId(), ''))
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
