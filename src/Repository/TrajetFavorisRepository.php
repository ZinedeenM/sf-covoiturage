<?php

namespace App\Repository;

use App\Entity\Lieu;
use App\Entity\TrajetFavoris;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrajetFavoris|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrajetFavoris|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrajetFavoris[]    findAll()
 * @method TrajetFavoris[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrajetFavorisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrajetFavoris::class);
    }

    // /**
    //  * @return TrajetFavoris[] Returns an array of TrajetFavoris objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TrajetFavoris
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
