<?php


namespace App\Helper;





use App\Entity\Lieu;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PlaceHelper
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    function placeFromString($stringifiedPlace){
        $response = $this->client->request('GET', 'https://api-adresse.data.gouv.fr/search/?q='.urlencode($stringifiedPlace).'&limit=1');

        $resp = json_decode($response->getContent());

        if ($response && $stringifiedPlace === $resp->features[0]->properties->label){
            $place = new Lieu();
            $place->setAdresse($resp->features[0]->properties->label);
            $place->setVille($resp->features[0]->properties->city);
            $place->setCodePostal($resp->features[0]->properties->postcode);

            return $place;
        }

        return false;
    }
}