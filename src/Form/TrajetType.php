<?php

namespace App\Form;

use App\Entity\Lieu;
use App\Entity\Trajet;
use App\Entity\User;
use App\Entity\Vehicule;
use App\Helper\PlaceHelper;
use App\Repository\TrajetRepository;
use App\Repository\VehiculeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrajetType extends AbstractType
{
    private $vehiculeRepo;

    public function __construct(VehiculeRepository $repository)
    {
        $this->vehiculeRepo = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('distance', NumberType::class, [
                "label" => "Distance à faire",
                "attr" => [
                    'hidden' => 'hidden'
                ],
            ])
            ->add('nbPassager',NumberType::class, [
                "label" => "Nombre de passager",
            ])
            ->add('dateDepart', DateTimeType::class, [
                "label" => "Date de départ",
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                "placeholder" => 'jj-mm-aa'
            ])
            ->add('heureDepart', TimeType::class, [
                'label' => 'Heure de départ',
            ])
            ->add('voiture', EntityType::class, [
                'data_class' => Vehicule::class,
                'class' => Vehicule::class,
                'choices' => $this->vehiculeRepo->findByUser($options['actualUser']),
                'choice_label' => function(Vehicule $vehicule){
                    return $vehicule->getModel() . ' - ' . $vehicule->getMarque();
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trajet::class,
            'compound' => true,
            'actualUser' => null
        ]);
    }
}
