<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewTrajetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('trajet', TrajetType::class, [
                'actualUser' => $options['actualUser'],
            ])
            ->add('lieuDepart', TextType::class, [
                'label' => 'Lieu de départ',
                'attr' => [
                    'class' => 'autocomplete-place',
                ]
            ])
            ->add('lieuArrivee', TextType::class, [
                'label' => 'Lieu d\'arrivé',
                'attr' => [
                    'class' => 'autocomplete-place',
                ]
            ])
            ->add('isFav', CheckboxType::class, [
                "label" => "Est-ce votre trajet favoris ?",
                "required" => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'actualUser' => null,
        ]);
    }
}
