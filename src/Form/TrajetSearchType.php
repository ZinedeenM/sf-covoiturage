<?php

namespace App\Form;


use Doctrine\DBAL\Types\TimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrajetSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lieuDepart', TextType::class, [
                'label' => 'Lieu de départ',
                'attr' => [
                    'class' => 'autocomplete-place',
                ]
            ])
            ->add('lieuArrivee', TextType::class, [
                'label' => 'Lieu d\'arrivé',
                'attr' => [
                    'class' => 'autocomplete-place',
                ]
            ])
            ->add('dateDepart', DateType::class, [
                "label" => "Date",
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                "placeholder" => 'jj-mm-aa'
            ])
            ->add('heureDepart', TimeType::class, [
                'label' => 'Heure de départ',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
