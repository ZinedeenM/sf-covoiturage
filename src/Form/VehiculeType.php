<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Vehicule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class VehiculeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('immatriculation', TextType::class, [
                "label" => "Immatriculation"
            ])
            ->add('consommation', NumberType::class, [
                "label" => "Consommation",
                "help" => "La consommation du vehicule au L/100km",
                "scale" => 2,
            ])
            ->add('model',TextType::class,[
                "label" => "Model"
            ])
            ->add('marque', TextType::class,[
                "label" => "Marque"
            ])
            ->add('nbPlaces', NumberType::class, [
                "label" => "Nombre de place"
            ])
            ->add('idUtilisateur', EntityType::class, [
                "label"=>"User",
                'class'=>User::class,
                'choice_label'=>function (User $utilisateur){
                    return $utilisateur->getPrenom()." ".$utilisateur->getNom();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vehicule::class,
        ]);
    }
}
