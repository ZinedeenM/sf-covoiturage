<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122155606 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO interet (id, nom, descriptif) VALUES
 ('1', 'Fûmeur', 'Lutilisateur fûme ou nest pas dérangé par les fumeurs'), ('2', 'Sociable', 'Lutilisateur aime sociabiliser et discuter'), ('3', 'Musique', 'Lutilisateur aime écouter de la musique sur la route');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
