<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201112105832 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE interet (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, descriptif VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE interet_user (interet_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_940627F6C1289ECC (interet_id), INDEX IDX_940627F6A76ED395 (user_id), PRIMARY KEY(interet_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lieu (id INT AUTO_INCREMENT NOT NULL, code_postal INT NOT NULL, adresse VARCHAR(255) NOT NULL, ville VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, destinataire_id INT NOT NULL, expediteur_id INT NOT NULL, contenu VARCHAR(255) NOT NULL, date_message DATETIME NOT NULL, INDEX IDX_B6BD307FA4F84F6E (destinataire_id), INDEX IDX_B6BD307F10335F61 (expediteur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trajet (id INT AUTO_INCREMENT NOT NULL, lieu_depart_id INT NOT NULL, lieu_arrivee_id INT NOT NULL, conducteur_id INT NOT NULL, distance DOUBLE PRECISION NOT NULL, nb_passager INT NOT NULL, date_depart DATE NOT NULL, prix DOUBLE PRECISION NOT NULL, INDEX IDX_2B5BA98CC16565FC (lieu_depart_id), INDEX IDX_2B5BA98CBF9A3FF6 (lieu_arrivee_id), INDEX IDX_2B5BA98CF16F4AC6 (conducteur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trajet_interet (trajet_id INT NOT NULL, interet_id INT NOT NULL, INDEX IDX_DB366ED1D12A823 (trajet_id), INDEX IDX_DB366ED1C1289ECC (interet_id), PRIMARY KEY(trajet_id, interet_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trajet_user (trajet_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_825A9176D12A823 (trajet_id), INDEX IDX_825A9176A76ED395 (user_id), PRIMARY KEY(trajet_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trajet_favoris (id INT AUTO_INCREMENT NOT NULL, id_utilisateur_id INT NOT NULL, lieu_depart_id INT NOT NULL, lieu_arrivee_id INT NOT NULL, reccurence VARCHAR(255) NOT NULL, INDEX IDX_FB84C546C6EE5C49 (id_utilisateur_id), INDEX IDX_FB84C546C16565FC (lieu_depart_id), INDEX IDX_FB84C546BF9A3FF6 (lieu_arrivee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATE NOT NULL, is_admin TINYINT(1) DEFAULT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicule (id INT AUTO_INCREMENT NOT NULL, id_utilisateur_id INT NOT NULL, immatriculation VARCHAR(255) NOT NULL, consommation DOUBLE PRECISION NOT NULL, model VARCHAR(255) NOT NULL, marque VARCHAR(255) NOT NULL, nb_places INT NOT NULL, INDEX IDX_292FFF1DC6EE5C49 (id_utilisateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE interet_user ADD CONSTRAINT FK_940627F6C1289ECC FOREIGN KEY (interet_id) REFERENCES interet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE interet_user ADD CONSTRAINT FK_940627F6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FA4F84F6E FOREIGN KEY (destinataire_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F10335F61 FOREIGN KEY (expediteur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trajet ADD CONSTRAINT FK_2B5BA98CC16565FC FOREIGN KEY (lieu_depart_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE trajet ADD CONSTRAINT FK_2B5BA98CBF9A3FF6 FOREIGN KEY (lieu_arrivee_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE trajet ADD CONSTRAINT FK_2B5BA98CF16F4AC6 FOREIGN KEY (conducteur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trajet_interet ADD CONSTRAINT FK_DB366ED1D12A823 FOREIGN KEY (trajet_id) REFERENCES trajet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trajet_interet ADD CONSTRAINT FK_DB366ED1C1289ECC FOREIGN KEY (interet_id) REFERENCES interet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trajet_user ADD CONSTRAINT FK_825A9176D12A823 FOREIGN KEY (trajet_id) REFERENCES trajet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trajet_user ADD CONSTRAINT FK_825A9176A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trajet_favoris ADD CONSTRAINT FK_FB84C546C6EE5C49 FOREIGN KEY (id_utilisateur_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trajet_favoris ADD CONSTRAINT FK_FB84C546C16565FC FOREIGN KEY (lieu_depart_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE trajet_favoris ADD CONSTRAINT FK_FB84C546BF9A3FF6 FOREIGN KEY (lieu_arrivee_id) REFERENCES lieu (id)');
        $this->addSql('ALTER TABLE vehicule ADD CONSTRAINT FK_292FFF1DC6EE5C49 FOREIGN KEY (id_utilisateur_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE interet_user DROP FOREIGN KEY FK_940627F6C1289ECC');
        $this->addSql('ALTER TABLE trajet_interet DROP FOREIGN KEY FK_DB366ED1C1289ECC');
        $this->addSql('ALTER TABLE trajet DROP FOREIGN KEY FK_2B5BA98CC16565FC');
        $this->addSql('ALTER TABLE trajet DROP FOREIGN KEY FK_2B5BA98CBF9A3FF6');
        $this->addSql('ALTER TABLE trajet_favoris DROP FOREIGN KEY FK_FB84C546C16565FC');
        $this->addSql('ALTER TABLE trajet_favoris DROP FOREIGN KEY FK_FB84C546BF9A3FF6');
        $this->addSql('ALTER TABLE trajet_interet DROP FOREIGN KEY FK_DB366ED1D12A823');
        $this->addSql('ALTER TABLE trajet_user DROP FOREIGN KEY FK_825A9176D12A823');
        $this->addSql('ALTER TABLE interet_user DROP FOREIGN KEY FK_940627F6A76ED395');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FA4F84F6E');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F10335F61');
        $this->addSql('ALTER TABLE trajet DROP FOREIGN KEY FK_2B5BA98CF16F4AC6');
        $this->addSql('ALTER TABLE trajet_user DROP FOREIGN KEY FK_825A9176A76ED395');
        $this->addSql('ALTER TABLE trajet_favoris DROP FOREIGN KEY FK_FB84C546C6EE5C49');
        $this->addSql('ALTER TABLE vehicule DROP FOREIGN KEY FK_292FFF1DC6EE5C49');
        $this->addSql('DROP TABLE interet');
        $this->addSql('DROP TABLE interet_user');
        $this->addSql('DROP TABLE lieu');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE trajet');
        $this->addSql('DROP TABLE trajet_interet');
        $this->addSql('DROP TABLE trajet_user');
        $this->addSql('DROP TABLE trajet_favoris');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE vehicule');
    }
}
