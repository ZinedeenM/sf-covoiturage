<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122212906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trajet_favoris DROP FOREIGN KEY FK_FB84C546C6EE5C49');
        $this->addSql('DROP INDEX IDX_FB84C546C6EE5C49 ON trajet_favoris');
        $this->addSql('ALTER TABLE trajet_favoris ADD user_id INT DEFAULT NULL, DROP id_utilisateur_id');
        $this->addSql('ALTER TABLE trajet_favoris ADD CONSTRAINT FK_FB84C546A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_FB84C546A76ED395 ON trajet_favoris (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trajet_favoris DROP FOREIGN KEY FK_FB84C546A76ED395');
        $this->addSql('DROP INDEX IDX_FB84C546A76ED395 ON trajet_favoris');
        $this->addSql('ALTER TABLE trajet_favoris ADD id_utilisateur_id INT NOT NULL, DROP user_id');
        $this->addSql('ALTER TABLE trajet_favoris ADD CONSTRAINT FK_FB84C546C6EE5C49 FOREIGN KEY (id_utilisateur_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_FB84C546C6EE5C49 ON trajet_favoris (id_utilisateur_id)');
    }
}
